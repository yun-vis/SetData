
source
https://en.wikipedia.org/wiki/Urea_cycle

Step	Reactants			Products				Catalyzed by		Location
1	NH3 + HCO3− + 2ATP		carbamoyl phosphate + 2ADP + Pi		CPS1			mitochondria
2	carbamoyl phosphate + ornithine	citrulline + Pi				OTC, zinc, biotin	mitochondria
3	citrulline + aspartate + ATP	argininosuccinate + AMP + PPi		ASS			cytosol
4	argininosuccinate		Arg + fumarate				ASL			cytosol
5	Arg + H2O			ornithine + urea			ARG1, manganese		cytosol


Parameter setting: