#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <vector>
#include <map>

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>

#include <tinyxml.h>

using namespace std;
using namespace boost::filesystem;

typedef boost::escaped_list_separator< char >
        BOOST_ESCAPED_LIST_SEP;
typedef boost::tokenizer< boost::escaped_list_separator< char > >
        BOOST_TOKENIZER_ESCAPED_LIST;

//------------------------------------------------------------------------------
//  Class
//------------------------------------------------------------------------------
class Position {

    public:
        string country;
        double x;
        double y;
};

class dataRow{

    public:

        unsigned int id;
        string week;
        int unsubscribed;
        int typeAH1;
        int typeAH3;
        int typeB;
        double percent;
        string source;
        string country;
};

class dataMatrix{

    public:

        unsigned int id;
        string country;
        vector< dataRow > data;

};

//------------------------------------------------------------------------------
//  Macro definitions
//------------------------------------------------------------------------------

#define THRESHOLD   (0)
#define	INFINITY		(1.0e+8)

#define MAX2( x, y )	(( (x)<(y) )? (y) : (x) )
#define MIN2( x, y )	(( (x)>(y) )? (y) : (x) )

//------------------------------------------------------------------------------
//  Global variables
//------------------------------------------------------------------------------

vector< dataMatrix > dmv;
string inputPath = "../../csv";

//string outputPath = "../../flu_full/";
//unsigned int beginID = 0;
//unsigned int endID = INFINITY;

string outputPath = "../../flu_6-8_46-48-2017/";
unsigned int beginID = 6;
unsigned int endID = 8+1;

vector< Position > pos;

//------------------------------------------------------------------------------
//  Variables for OpenGL functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  Main drawing functions
//------------------------------------------------------------------------------
void setPosition( void )
{
    pos.resize( dmv.size() );

    for( unsigned int i = 0; i < dmv.size(); i++ ){
        pos[i].country = dmv[i].country;

        //cerr << i << " = " << pos[i].country << endl;

        if( pos[i].country.compare( "Austria" ) == true ){
            pos[i].x = 9.391285;
            pos[i].y = 47.438971;
        }
        if( pos[i].country.compare( "Belgium" ) == true ) {
            pos[i].x = -0.762363;
            pos[i].y = 50.631755;
        }
        if( pos[i].country.compare( "Czech Republic" ) == true ){
            pos[i].x = 10.557297;
            pos[i].y = 49.798358;
        }
        if( pos[i].country.compare( "France") == true ) {
            pos[i].x = -2.288479;
            pos[i].y = 46.952501;
        }
        if( pos[i].country.compare( "Germany" ) == true ) {
            pos[i].x = 5.009428;
            pos[i].y = 51.037082;
        }
        if( pos[i].country.compare( "Ireland" ) == true ) {
            pos[i].x = -9.627455;
            pos[i].y = 53.256336;
        }
        if( pos[i].country.compare( "Luxembourg" ) == true ) {
            pos[i].x = 1.148324;
            pos[i].y = 49.643503;
        }
        if( pos[i].country.compare( "Netherlands" ) == true ) {
            pos[i].x =  1.492135;
            pos[i].y = 52.168567;
        }
        if( pos[i].country.compare( "Norway" ) == true ) {
            pos[i].x = 6.912129;
            pos[i].y = 62.331459;
        }
        if( pos[i].country.compare( "Poland" ) == true ) {
            pos[i].x = 13.702939;
            pos[i].y = 52.273199;
        }
        if( pos[i].country.compare( "Sweden" ) == true ) {
            pos[i].x = 10.017605;
            pos[i].y = 60.317823;
        }
        if( pos[i].country.compare( "United Kingdom - England" ) == true ) {
            pos[i].x = -5.077763;
            pos[i].y = 51.938954;
        }
    }
}

void loadData( const string & fileName )
{
    cerr << "fileName: " << fileName << endl;
    std::ifstream ifs( fileName );

    dataMatrix dm;

    dm.id = dmv.size();
    string line;
    getline( ifs, line ); // skip the first line

    int index = 0;
    while( getline( ifs, line ) ) {

        vector< string > vec;
        BOOST_TOKENIZER_ESCAPED_LIST tk( line );//, BOOST_ESCAPED_LIST_SEP( '\\', ',', '\"') );

        BOOST_FOREACH( std::string s, tk ) {

            if( s.length() == 0 ) s = "0";

            string copy = s;
            vec.push_back( copy );
            //cerr << "copy = " << vec[ vec.size()-1 ]  << endl;
        }

#ifdef DEBUG
        cerr << "vecsize = " << vec.size() << endl;
        for( unsigned int i = 0; i < vec.size(); i++ ){
            cerr << " " << vec[ i ];
        }
        cerr << endl;
#endif // DEBUG

        // update data matrix
        dataRow dr;
        dr.id = index;
        dr.week         = vec[ 0 ];
        dr.unsubscribed = atoi( vec[ 1 ].c_str() );
        dr.typeAH1      = atoi( vec[ 2 ].c_str() );
        dr.typeAH3      = atoi( vec[ 3 ].c_str() );
        dr.typeB        = atoi( vec[ 4 ].c_str() );
        dr.percent      = atof( vec[ 5 ].c_str() );
        dr.source       = vec[ 6 ];
        dr.country      = vec[ 7 ];

        dm.data.push_back( dr );
        dm.country = vec[ 7 ];
        index++;

        //cerr << "id = " << dr.id << " ?= " << dm.data[ dm.data.size()-1 ].id << endl;
    }

#ifdef DEBUG
    cerr << "id = " << dm.id << endl;
    cerr << "country = " << dm.country << endl;
    cerr << "rownum = " << dm.data.size() << endl;
    cerr << dm.data[ 0 ].week << endl;
#endif // DEBUG

    dmv.push_back( dm );

#ifdef DEBUG
    for( unsigned int i = 0; i < dm.data.size(); i++ ){

        cerr <<
            dm.data[i].id           << " " <<
            dm.data[i].week         << " " <<
            dm.data[i].unsubscribed << " " <<
            dm.data[i].typeAH1      << " " <<
            dm.data[i].typeAH3      << " " <<
            dm.data[i].typeB        << " " <<
            dm.data[i].percent      << " " <<
            dm.data[i].source       << " " <<
            dm.data[i].country      << endl;

    }
#endif // DEBUG

}

void loadCSV( void )
{
    path p{ inputPath.c_str() };
    //cout << "path: " << p.string() << endl;

    if( is_directory( p ) ) {

        cout << p << " is a directory containing:" << endl;

        vector< path > vec;             // store paths,
        copy( directory_iterator( p ), directory_iterator(), back_inserter( vec ) );

        for( vector< path >::const_iterator it = vec.begin(); it != vec.end(); it ++ )
        {
            loadData( it->string() );
        }
    }
}

void exportXML( unsigned int lID, unsigned int initID )
{
    // string outputPath = "../../xml/";

    TiXmlDocument xmlDoc;
    TiXmlNode *nDataSet = xmlDoc.InsertEndChild( TiXmlElement( "Dataset" ) );

    // Elements
    TiXmlNode* elementNode = new TiXmlElement( "Elements" );

    for( unsigned int i = 0; i < dmv.size(); i++ ){

        TiXmlElement* elementElement = new TiXmlElement( "Element" );

        elementElement->SetAttribute( "ID", i );
        elementElement->SetAttribute( "PosX", to_string( pos[i].x ) );
        elementElement->SetAttribute( "PosY", to_string( pos[i].y ) );
        // elementElement->SetAttribute( "Image", "/country/" + pos[i].country + ".jpg" );
        // elementElement->SetAttribute( "Image", "/country/" + to_string( i ) + ".jpg" );

        if( pos[i].country.compare( "Austria" ) == true ){
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/austria.jpg" );
        }
        if( pos[i].country.compare( "Belgium" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/belgium.jpg" );
        }
        if( pos[i].country.compare( "Czech Republic" ) == true ){
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/czech.jpg" );
        }
        if( pos[i].country.compare( "France" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/france.jpg" );
        }
        if( pos[i].country.compare( "Germany" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/germany.jpg" );
        }
        if( pos[i].country.compare( "Ireland" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/ireland.jpg" );
        }
        if( pos[i].country.compare( "Luxembourg" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/luxembourg.jpg" );
        }
        if( pos[i].country.compare( "Netherlands" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/netherlands.jpg" );
        }
        if( pos[i].country.compare( "Norway" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/norway.jpg" );
        }
        if( pos[i].country.compare( "Poland" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/poland.jpg" );
        }
        if( pos[i].country.compare( "Sweden" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/sweden.jpg" );
        }
        if( pos[i].country.compare( "United Kingdom - England" ) == true ) {
            elementElement->SetAttribute( "Image", "/Users/yun/Desktop/gitroot/data/SetData/flu/icons/uk-england.jpg" );
        }

        //cerr << i << " = " << pos[i].country << elementElement->Attribute( "Image" ) << endl;
        elementNode->InsertEndChild( *elementElement );
    }
    nDataSet->InsertEndChild( *elementNode );
    // cerr << endl;

    // Sets
    TiXmlNode* setNode = new TiXmlElement( "Sets" );

    for( unsigned int i = 0; i < 4; i++ ) {

        TiXmlElement *setElement = new TiXmlElement( "Set" );

        setElement->SetAttribute( "ID", i );
        double rgba[4];

        if( i == 0 ) {
            rgba[ 0 ] = 1; rgba[ 1 ] = 0; rgba[ 2 ] = 0; rgba[ 3 ] = 1;
        }
        else if( i == 1 ) {
            rgba[ 0 ] = 0; rgba[ 1 ] = 1; rgba[ 2 ] = 0; rgba[ 3 ] = 1;
        }
        else if( i == 2 ) {
            rgba[ 0 ] = 0; rgba[ 1 ] = 0; rgba[ 2 ] = 1; rgba[ 3 ] = 1;
        }
        else if( i == 3 ) {
            rgba[ 0 ] = 1; rgba[ 1 ] = 1; rgba[ 2 ] = 0; rgba[ 3 ] = 1;
        }
        else if( i == 4 ) {
            rgba[ 0 ] = 0; rgba[ 1 ] = 1; rgba[ 2 ] = 1; rgba[ 3 ] = 1;
        }
        else {
            rgba[ 0 ] = 0; rgba[ 1 ] = 0; rgba[ 2 ] = 0; rgba[ 3 ] = 1;
        }

        setElement->SetAttribute( "ColorR", rgba[0] );
        setElement->SetAttribute( "ColorG", rgba[1] );
        setElement->SetAttribute( "ColorB", rgba[2] );
        setElement->SetAttribute( "ColorA", rgba[3] );

        setNode->InsertEndChild( *setElement );
    }
    nDataSet->InsertEndChild( *setNode );

    // ElementsOfSet
    TiXmlNode* elementsOfSetNode = new TiXmlElement( "ElementsOfSet" );

    for( unsigned int k = 0; k < 4; k++ ){ // unsubscribed, AH1, AH3, B
        TiXmlElement* setElement2 = new TiXmlElement( "Set" );

        setElement2->SetAttribute( "ID", k );

        for( unsigned int i = 0; i < dmv.size() ; i++ ){

            if( ( k == 0 && dmv[ i ].data[ lID+initID ].unsubscribed > THRESHOLD ) ||
                ( k == 1 && dmv[ i ].data[ lID+initID ].typeAH1 > THRESHOLD ) ||
                ( k == 2 && dmv[ i ].data[ lID+initID ].typeAH3 > THRESHOLD ) ||
                ( k == 3 && dmv[ i ].data[ lID+initID ].typeB > THRESHOLD ) ){
                TiXmlElement* setEle2 = new TiXmlElement( "Element" );
                setEle2->SetAttribute( "ID", i );
                setElement2->InsertEndChild( *setEle2 );
            }
        }

        elementsOfSetNode->InsertEndChild( *setElement2 );
    }

    nDataSet->InsertEndChild( *elementsOfSetNode );

    ostringstream       ostr;
    ostr.str("");
    ostr << setw( 4 ) << setfill( '0' ) << to_string( lID );

    //string fileName = outputPath + week + id + ".xml";
    string fileName = outputPath + "flu-anim." + ostr.str() + ".xml";
    xmlDoc.SaveFile( fileName.c_str() );
}


void exportXMLList( void )
{
    //cerr << "size = " << dmv[0].data.size() << endl;
    unsigned int id = 0;
    //unsigned int initID = 6;
    //unsigned int endID = 9;
    for( unsigned int i = beginID; i < MIN2(dmv[0].data.size(), endID ); i++ ){
    // for( unsigned int i = 0; i < dmv[0].data.size(); i++ ){
        exportXML( id, beginID );
        id++;
    }
}

//------------------------------------------------------------------------------
//  Main function
//------------------------------------------------------------------------------
int main( int argc, char **argv )
{
    // initialization
    dmv.clear();

    loadCSV();
    setPosition();
    exportXMLList();

#ifdef DEBUG
    for( unsigned int k = 0; k < dmv.size(); k++ ) {

        cerr << endl << " id = " << dmv[k].id << " country = " << dmv[k].country << endl;
        for( unsigned int i = 0; i < dmv[ k ].data.size(); i++ ){

            cerr <<
                 dmv[k].data[i].id           << " " <<
                 dmv[k].data[i].week         << " " <<
                 dmv[k].data[i].unsubscribed << " " <<
                 dmv[k].data[i].typeAH1      << " " <<
                 dmv[k].data[i].typeAH3      << " " <<
                 dmv[k].data[i].typeB        << " " <<
                 dmv[k].data[i].percent      << " " <<
                 dmv[k].data[i].source       << " " <<
                 dmv[k].data[i].country      << endl;

        }
    }
#endif // DEBUG

    cerr << "The program finished successfully..." << endl;

    return 0;
}
