//==============================================================================
// Common.cc
//	: macro definitions common to this package
//
//==============================================================================

#ifndef _Common_H               // beginning of header file
#define _Common_H               // notifying that this file is included

#include <map>
#include <vector>
#include <sstream>


//------------------------------------------------------------------------------
//	Macro Switches
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	Macro definitions
//------------------------------------------------------------------------------
#ifndef TRUE
#define TRUE			(1)
#endif	// TRUE
#ifndef FALSE
#define FALSE			(0)
#endif	// FALSE

#define MAX_STR			(256)

#ifndef SQRT2
#define SQRT2			(1.414213562)
#endif	// SQRT2
#define TWOPI			(2.0*M_PI)

#ifndef INFINITY
#define	INFINITY		(1.0e+8)
#endif	// INFINITY

#ifndef MININUM_INTEGER
#define MINIMUM_INTEGER (-2147483648)
#endif	// MINIMUM_INTEGER

#ifndef MAXIMUM_INTEGER
#define MAXIMUM_INTEGER (2147483647)
#endif	// MAXIMUM_INTEGER

#ifndef USER_DEFINDED_EPS
#define	USER_DEFINDED_EPS			(1.0e-8)
#endif	// USER_DEFINDED_EPS

#ifndef ROUND_EPS
#define	ROUND_EPS		(1.0e-4)
#endif	// ROUND_EPS

#define MAX2( x, y )	(( (x)<(y) )? (y) : (x) )
#define MIN2( x, y )	(( (x)>(y) )? (y) : (x) )

#ifndef ABS
#define ABS( x )		(( (x)>0 )? (x) : (-(x)) )
#endif //ABS

#define SQUARE( x )	((x)*(x))

#define FLIP( type, a, b )	{ type temp = a; a = b; b = temp; }


#define DIM_OF_SPACE	(2)
#define NO_INDEX	(-1)
#define NO_UNSIGNED_ID	(65536)

#define BUFFER_SIZE	(256)

//#define DEFAULT_WIDTH   (768)
//#define DEFAULT_HEIGHT        (768)
#define DEFAULT_WIDTH (1280)
#define DEFAULT_HEIGHT        (960)
//#define DEFAULT_WIDTH (1532)
//#define DEFAULT_HEIGHT        (1149)


#endif // _Common_H
